﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utilities.Input;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Controller_Games
{

    class InputActions : InputManager
    {
        public enum Action
        {
            Fire,
            Up,
            Down,
            Left,
            Right,
            TotalActionCount,
        }

        private static readonly string[] actionNames =
        {
            "Fire",
            "Move/Menu Up",
            "Move/Menu Down",
            "Move/Menu Left",
            "Move/Menu Right",
        };

        public class ActionMap
        {
            public Nullable<Utilities.Input.GamePadButtons> gamePadButton;
            public Nullable<Keys> key;
            public Nullable<MouseButtons> mouseButton;
        }
        
        private static ActionMap[] actionMapsPlayerOne;
        private static ActionMap[] actionMapsPlayerTwo;
        private static ActionMap[] actionMapsPlayerThree;
        private static ActionMap[] actionMapsPlayerFour;

        public InputActions(Game game)
            : base(game)
        {
            
        }

        public InputActions(Game game, bool lockMouse)
            :base(game, lockMouse)
        {

        }

        public override void Initialize()
        {
            ResetActionMaps();
            base.Initialize();
        }

        /// <summary>
        /// Resets the action mappings for each player.
        /// </summary>
        private static void ResetActionMaps()
        {
            actionMapsPlayerOne = new ActionMap[(int)Action.TotalActionCount];
            actionMapsPlayerTwo = new ActionMap[(int)Action.TotalActionCount];
            actionMapsPlayerThree = new ActionMap[(int)Action.TotalActionCount];
            actionMapsPlayerFour = new ActionMap[(int)Action.TotalActionCount];
            ResetActionMap(actionMapsPlayerOne);
            ResetActionMap(actionMapsPlayerTwo);
            ResetActionMap(actionMapsPlayerThree);
            ResetActionMap(actionMapsPlayerFour);
        }

        /// <summary>
        /// Helper function to reset each player's action maps.
        /// </summary>
        /// <param name="actionMap">The player's action map to reset.</param>
        private static void ResetActionMap(ActionMap[] actionMap)
        {
            actionMap[(int)Action.Fire].mouseButton = MouseButtons.Left;
            actionMap[(int)Action.Fire].gamePadButton = Utilities.Input.GamePadButtons.RightTrigger;

            actionMap[(int)Action.Up].key = Keys.W;
            actionMap[(int)Action.Up].gamePadButton = Utilities.Input.GamePadButtons.Up;

            actionMap[(int)Action.Down].key = Keys.S;
            actionMap[(int)Action.Down].gamePadButton = Utilities.Input.GamePadButtons.Down;

            actionMap[(int)Action.Left].key = Keys.A;
            actionMap[(int)Action.Left].gamePadButton = Utilities.Input.GamePadButtons.Left;

            actionMap[(int)Action.Right].key = Keys.D;
            actionMap[(int)Action.Right].gamePadButton = Utilities.Input.GamePadButtons.Right;
        }

        #region Action Accessors
        public static string GetActionName(Action action)
        {
            int index = (int)action;

            if ((index < 0) || (index > actionNames.Length))
            {
                throw new ArgumentException("action");
            }

            return actionNames[index];
        }

        public static bool IsActionPressed(Action action, PlayerIndex player)
        {
            switch (player)
            {
                case PlayerIndex.One: return IsActionMapPressed(actionMapsPlayerOne[(int)action], player);
                case PlayerIndex.Two: return IsActionMapPressed(actionMapsPlayerTwo[(int)action], player);
                case PlayerIndex.Three: return IsActionMapPressed(actionMapsPlayerThree[(int)action], player);
                case PlayerIndex.Four: return IsActionMapPressed(actionMapsPlayerFour[(int)action], player);
            }
            return false;
        }

        public static bool IsActionTriggered(Action action, PlayerIndex player)
        {
            switch(player)
            {
                case PlayerIndex.One: return IsActionMapTriggered(actionMapsPlayerOne[(int)action], player);
                case PlayerIndex.Two: return IsActionMapTriggered(actionMapsPlayerTwo[(int)action], player);
                case PlayerIndex.Three: return IsActionMapTriggered(actionMapsPlayerThree[(int)action], player);
                case PlayerIndex.Four: return IsActionMapTriggered(actionMapsPlayerFour[(int)action], player);
            }
            return false;
        }

        private static bool IsActionMapPressed(ActionMap actionMap, PlayerIndex player)
        {
            bool pressed = false;
            if (actionMap.key != null && IsKeyPressed(actionMap.key.Value)) { pressed = true; }
            if (actionMap.gamePadButton != null && IsGamePadButtonPressed(actionMap.gamePadButton.Value, player)) { pressed = true; }
            if (actionMap.mouseButton != null && IsMouseButtonPressed(actionMap.mouseButton.Value)) { pressed = true; }
            return pressed;
        }

        private static bool IsActionMapTriggered(ActionMap actionMap, PlayerIndex player)
        {
            bool pressed = false;
            if (actionMap.key != null && IsKeyTriggered(actionMap.key.Value)) { pressed = true; }
            if (actionMap.gamePadButton != null && IsGamePadButtonTriggered(actionMap.gamePadButton.Value, player)) { pressed = true; }
            if (actionMap.mouseButton != null && IsMouseButtonTriggered(actionMap.mouseButton.Value)) { pressed = true; }
            return pressed;
        }
        #endregion
    }
}
