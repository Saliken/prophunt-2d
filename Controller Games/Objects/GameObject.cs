﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using FarseerPhysics;

namespace Controller_Games.Objects
{
    /// <summary>
    /// A middle-man class that holds all the data that describes a GameObject.
    /// Used in saving/loading maps.
    /// </summary>
    class GameObjectInfo
    {
        public string texName;
        public int health;
        public int xPos;
        public int yPos;
        public float density;
        public float rotation;
        public int speed;

        //Rectangle Body
        public int width;
        public int height;

        //Circle Body
        public float radius;
    }

    /// <summary>
    /// An object that exists in the game world.
    /// </summary>
    class GameObject
    {
        public Texture2D texture;
        public int health;
        public int speed;
        public Body physicsBody;

        public GameObject(Body body, Texture2D texture)
        {
            this.texture = texture;
            this.physicsBody = body;
        }

    }
}
