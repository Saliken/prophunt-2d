﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using FarseerPhysics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace Controller_Games.Objects
{

    /// <summary>
    /// A factory class that deals with creating GameObjects.
    /// </summary>
    static class ObjectFactory
    {
        /// <summary>
        /// Creates a new GameObject from GameInfo.
        /// </summary>
        /// <param name="info">The GameObjectInfo that describes the object.</param>
        /// <param name="world">The FarseerPhysics world to put the new GameObject in.</param>
        /// <param name="content">The ContentManager to load content from.</param>
        /// <returns></returns>
        public static GameObject CreateGameObject(GameObjectInfo info, World world, ContentManager content)
        {
            GameObject obj;
            obj = new GameObject(GetBodyFromInfo(info, world), content.Load<Texture2D>("objects/" + info.texName));
            obj.physicsBody.UserData = obj;
            obj.speed = info.speed;
            return obj;
        }


        public static GameObject CreateGameObject(World world, Vector2 position, float rotation, int width, int height, float density, Texture2D texture)
        {
            GameObject obj;
            Body body = BodyFactory.CreateRectangle(world, ConvertUnits.ToSimUnits(width), ConvertUnits.ToSimUnits(height), density, ConvertUnits.ToSimUnits(position));
            obj = new GameObject(body, texture);
            obj.physicsBody.UserData = obj;
            body.BodyType = BodyType.Dynamic;
            return obj;
        }

        /// <summary>
        /// Creates a new GameObject by cloning an existing GameObject.
        /// </summary>
        /// <param name="objToClone">The GameObject to clone.</param>
        /// <param name="world">The FarseerPhysics world to add the new GameObject to.</param>
        /// <param name="currentObj">The current GameObject, to be turned into the new one.</param>
        /// <returns></returns>
        public static GameObject CloneGameObject(GameObject objToClone, World world, GameObject currentObj)
        {
            Body newBody = objToClone.physicsBody.DeepClone();
            newBody.SetTransform(currentObj.physicsBody.Position, currentObj.physicsBody.Rotation);
            world.RemoveBody(currentObj.physicsBody);
            /*Vector2 pos = currentObj.physicsBody.Position;
            float rot = currentObj.physicsBody.Rotation;
            world.RemoveBody(currentObj.physicsBody); //Remove the current body from the world.

            Body body = new Body(world, currentObj.physicsBody.Position, currentObj.physicsBody.Rotation, currentObj); //Create a new body and add it to the world.
            body.BodyType = BodyType.Dynamic;
            objToClone.physicsBody.FixtureList[0].CloneOnto(body); //Attach the fixture info from the object to clone, onto the new object.

            body.AngularDamping = objToClone.physicsBody.AngularDamping;
            body.LinearDamping = objToClone.physicsBody.LinearDamping;*/
            GameObject obj = new GameObject(newBody, objToClone.texture);
            obj.speed = objToClone.speed;
            return obj;
        }

        private static Body GetBodyFromInfo(GameObjectInfo info, World world)
        {
            Body body = new Body(world, ConvertUnits.ToSimUnits(new Vector2(info.xPos, info.yPos)), info.rotation);
            body.LinearDamping = 10;
            body.AngularDamping = 8;
            if (info.radius != 0) //Cicle Shape
            {
                FixtureFactory.AttachCircle(ConvertUnits.ToSimUnits(info.radius), info.density, body);
            }
            else //Rectangle Shape
            {
                FixtureFactory.AttachRectangle(ConvertUnits.ToSimUnits(info.width), ConvertUnits.ToSimUnits(info.height), info.density, Vector2.Zero, body);
            }
            body.BodyType = BodyType.Dynamic;
            return body;
        }
    }
}
