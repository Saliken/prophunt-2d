﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.IO;

namespace Controller_Games.Maps
{
    /// <summary>
    /// A factory class that deals with deserializing .json maps.
    /// </summary>
    static class MapFactory
    {
        public static List<string> mapNames;
        private static bool initialized = false;

        public static void Initialize()
        {
            GetAllMapNames();
            initialized = true;
        }

        /// <summary>
        /// Get's the list of all map files in the content directory.
        /// </summary>
        private static void GetAllMapNames()
        {
            mapNames = Directory.GetFiles("Content", "*.json").Select(path => Path.GetFileName(path)).ToList();
            for(int i = 0; i < mapNames.Count; i++)
            {
                Console.WriteLine(mapNames[i]);
            }
        }

        /// <summary>
        /// Takes a .json map file and deserializes it back into a map.
        /// </summary>
        /// <param name="filename">The filename of the map.</param>
        /// <returns>A working map.</returns>
        public static Map GetMap(string filename)
        {
            if (initialized != true) Initialize();

            Map map = new Map();
            map = JsonConvert.DeserializeObject<Map>(File.ReadAllText("Content/" + filename));
            map.CreateObjectsFromInfo();
            return map;
        }
    }
}
