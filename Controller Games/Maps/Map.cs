﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.IO;
using Controller_Games.Objects;
using FarseerPhysics.Dynamics;
using FarseerPhysics;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Controller_Games.Maps
{
    class Map
    {
        public string name;
        public int hunterWins;
        public int propWins;
        public List<GameObjectInfo> objectInfos;
        private List<GameObject> objects;
        public World world;
        public string mapTextureName;
        public Texture2D texture;

        public Map()
        {
            objectInfos = new List<GameObjectInfo>();
            objects = new List<GameObject>();
            world = new World(new Vector2(0f, 0f));
        }

        public void CreateObjectsFromInfo()
        {
            //Takes all our object information and turns it into actual physical objects.
            for(int i = 0; i < objectInfos.Count; i++)
            {
                objects.Add(ObjectFactory.CreateGameObject(objectInfos[i], world, Game1.content));
            }

            texture = Game1.content.Load<Texture2D>("maps/" + mapTextureName);

        }

        public void Update(GameTime gameTime)
        {
            /*double dt = 1f / 120f;
            double frameTime = gameTime.ElapsedGameTime.TotalSeconds;
            while(frameTime > 0.0)
            {
                float deltaTime = (float)Math.Min(frameTime, dt); //Use either the fixed step or the remainder of frameTime.
                world.Step(deltaTime);
                frameTime -= deltaTime;
            }*/
            world.Step((float)Math.Min(gameTime.ElapsedGameTime.TotalSeconds, 1f / 60f));
        }

        public void Draw(SpriteBatch SB)
        {
            //Draw the map texture

            //Draw the players

            //Draw Objects
            for (int i = 0; i < objects.Count; i++)
            {
                SB.Draw(objects[i].texture, ConvertUnits.ToDisplayUnits(objects[i].physicsBody.Position), null, Color.White, objects[i].physicsBody.Rotation, new Vector2(objects[i].texture.Width / 2f, objects[i].texture.Height / 2f), 1f, SpriteEffects.None, 1f);
            }
        }

        public void SaveJson()
        {
            File.WriteAllText("Content/" + name +".json", JsonConvert.SerializeObject(this));
        }
    }
}
