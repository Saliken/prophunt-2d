﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using FarseerPhysics;
using Utilities.Input;
using Controller_Games.Objects;

namespace Controller_Games.Characters
{
    class Character
    {
        public Texture2D texture;
        //public Body physicsBody;
        public PlayerIndex player;
        public GameObject currentObject;

        public Character(World world, Vector2 position, Texture2D texture, PlayerIndex player)
        {
            this.player = player;
            this.texture = texture;
            currentObject = ObjectFactory.CreateGameObject(world, position, 0f, texture.Width - 4, texture.Height - 4, 1f, texture);
            currentObject.physicsBody.LinearDamping = 15;
            currentObject.physicsBody.AngularDamping = 11;
            currentObject.speed = 200;
        }

        public void Update(GameTime GT)
        {
            if (InputManager.IsGamePadEnabled(player))
            {
                bool rotate = true;
              
                Vector2 moveVec2 = InputManager.GamePadRightStickValue(player);
                
                if (moveVec2.Length() < 0.5f) rotate = false;
                else moveVec2.Normalize();
                moveVec2 *= 5;
                moveVec2.X = 0 - moveVec2.X;
                moveVec2 += ConvertUnits.ToDisplayUnits(currentObject.physicsBody.Position);
                if(rotate) FarseerPhysics.Utilities.RotateTowardsPoint(currentObject.physicsBody, moveVec2);
            }
        }

        /// <summary>
        /// Moves the player in the direction given.
        /// </summary>
        /// <param name="direction">The direction, expressed as a normalized vector.</param>
        public void Move(Vector2 direction, GameTime GT)
        {
            direction.Y = -direction.Y;
            currentObject.physicsBody.ApplyLinearImpulse(ConvertUnits.ToSimUnits(direction * ((currentObject.speed*(currentObject.physicsBody.Mass* 15)) * (float)GT.ElapsedGameTime.TotalSeconds)));
        }

        /// <summary>
        /// Rotates the player towards a target vector.
        /// </summary>
        /// <param name="target">The vector location.</param>
        public void Rotate(Vector2 target, GameTime GT)
        {
            if (Vector2.Distance(target, ConvertUnits.ToDisplayUnits(currentObject.physicsBody.Position)) > 0f) // Stops rotation when the target point is too near (jitter).
            {
                currentObject.physicsBody.ApplyAngularImpulse(FarseerPhysics.Utilities.RotateTowardsPoint(currentObject.physicsBody, target));
            }
        }

        public void Draw(SpriteBatch SB)
        {
            SB.Draw(currentObject.texture, ConvertUnits.ToDisplayUnits(currentObject.physicsBody.Position), null, Color.White, currentObject.physicsBody.Rotation, new Vector2(currentObject.texture.Width /2f, currentObject.texture.Height / 2f), 1f, SpriteEffects.None, 1f);
        }
    }
}
