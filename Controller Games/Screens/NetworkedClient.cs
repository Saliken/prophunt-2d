﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lidgren.Network;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Utilities.ScreenSystem;

namespace Controller_Games.Screens
{
    class NetworkedClient : Screen
    {
        NetPeerConfiguration config;
        NetClient client;

        public NetworkedClient()
        {
            config = new NetPeerConfiguration("PH2D");

            client = new NetClient(config);
        }

        public override void Init()
        {
            client.Start();
        }

        public override void Update(GameTime gameTime)
        {
            NetIncomingMessage msg;
            while ((msg = client.ReadMessage()) != null)
            {


            }
        }

        public override void Draw(SpriteBatch SB)
        {
            throw new NotImplementedException();
        }
    }
}
