﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Utilities.ScreenSystem;
using FarseerPhysics.Dynamics;
using FarseerPhysics;
using Controller_Games.Maps;
using Controller_Games.Characters;
using Utilities.Input;
using Utilities;
using Controller_Games.Objects;

namespace Controller_Games.Screens
{
    class TestingScreen : Screen
    {
        private Map testMap;

        private List<Prop> props;
        private List<Hunter> hunters;

        private Viewport defaultView;
        private Viewport playerOneView;
        private Viewport playerTwoView;

        private Camera2D playerOneCam;
        private Camera2D playerTwoCam;

        public TestingScreen()
        {
            testMap = MapFactory.GetMap("MapWithObjects.json");
            props = new List<Prop>();
            hunters = new List<Hunter>();

            props.Add(new Prop(testMap.world, new Vector2(200, 100), Game1.content.Load<Texture2D>("objects/Crate1"), PlayerIndex.One));
            props.Add(new Prop(testMap.world, new Vector2(400, 100), Game1.content.Load<Texture2D>("objects/Crate2"), PlayerIndex.Two));
            props.Add(new Prop(testMap.world, new Vector2(600, 100), Game1.content.Load<Texture2D>("objects/Crate1"), PlayerIndex.Three));

            playerOneCam = new Camera2D();
            playerTwoCam = new Camera2D();
            
        }

        public override void Init()
        {
            //Setup viewports.
            defaultView = game.GraphicsDevice.Viewport;
            playerOneView = game.GraphicsDevice.Viewport;
            playerTwoView = playerOneView;
            playerOneView.Width /= 2;
            playerTwoView.Width /= 2;
            playerTwoView.X = playerOneView.Width;
            playerOneCam.ViewportHeight = playerOneView.Height;
            playerOneCam.ViewportWidth = playerOneView.Width;
            playerTwoCam.ViewportHeight = playerTwoView.Height;
            playerTwoCam.ViewportWidth = playerTwoView.Width;
        }

        private void PlayerInput()
        {

        }

        private void UpdatePlayers(GameTime GT)
        {
            for(int i = 0; i < props.Count; i++)
            {
                //Update Props
                UpdatePlayerMovement(GT, props[i]);
            }

            for(int i = 0; i < hunters.Count; i++)
            {
                //Update Hunters
                //UpdatePlayerMovement(GT, hunters[i]);
            }
            
        }

        private void UpdatePlayerMovement(GameTime GT, Character player)
        {
            Vector2 thumbStickL = InputManager.GamePadLeftStickValue(player.player);
            Vector2 thumbStickR = InputManager.GamePadRightStickValue(player.player);

            //Movement
            if (thumbStickL.Length() != 0) thumbStickL.Normalize();
            player.Move(thumbStickL, GT);

            //Rotation
            thumbStickR.X = -thumbStickR.X; //Flip the sign to work with Farseer.
            if (thumbStickR.LengthSquared() > 0.9f)
            {
                Vector2 targetPos = ConvertUnits.ToDisplayUnits(player.currentObject.physicsBody.Position) + (thumbStickR);
                player.Rotate(targetPos, GT);
            }

            
        }

        private GameObject RayCast(Vector2 point1, Vector2 point2, Fixture playerFix)
        {
            GameObject objHit = null;
            testMap.world.RayCast((f, p, n, fr) =>
            {
                if(f == playerFix)
                {
                    return -1;
                }
                objHit = (GameObject)f.Body.UserData;
                return fr;
            }, point1, point2);

            return objHit;
        }

        public override void Update(GameTime gameTime)
        {
            UpdatePlayers(gameTime);
            testMap.Update(gameTime);
            if(InputManager.IsGamePadRightTriggerTriggered(PlayerIndex.One))
            {
                Vector2 direction;
                direction.X = (float)Math.Sin(props[0].currentObject.physicsBody.Rotation);
                direction.Y = (float)Math.Cos(props[0].currentObject.physicsBody.Rotation);
                direction.Y = -direction.Y;
                if (direction.Length() > 0) direction.Normalize();
                direction *= 200;
                
                GameObject newPos = RayCast(props[0].currentObject.physicsBody.Position, ConvertUnits.ToSimUnits(direction) + props[0].currentObject.physicsBody.Position, props[0].currentObject.physicsBody.FixtureList[0]);
                if(newPos != null) props[0].currentObject = Objects.ObjectFactory.CloneGameObject(newPos, testMap.world, props[0].currentObject);
            }

            if (InputManager.IsGamePadRightTriggerTriggered(PlayerIndex.Two))
            {
                Vector2 direction;
                direction.X = (float)Math.Sin(props[1].currentObject.physicsBody.Rotation);
                direction.Y = (float)Math.Cos(props[1].currentObject.physicsBody.Rotation);
                direction.Y = -direction.Y;
                if (direction.Length() > 0) direction.Normalize();
                direction *= 200;

                GameObject newPos = RayCast(props[1].currentObject.physicsBody.Position, ConvertUnits.ToSimUnits(direction) + props[1].currentObject.physicsBody.Position, props[1].currentObject.physicsBody.FixtureList[0]);
                if (newPos != null) props[1].currentObject = Objects.ObjectFactory.CloneGameObject(newPos, testMap.world, props[1].currentObject);
            }

            if (InputManager.IsGamePadLeftShoulderTriggered(PlayerIndex.One)) playerOneCam.AdjustZoom(-0.1f);
            if (InputManager.IsGamePadRightShoulderTriggered(PlayerIndex.One)) playerOneCam.AdjustZoom(0.1f);
            playerOneCam.CenterOn(ConvertUnits.ToDisplayUnits(props[0].currentObject.physicsBody.Position));
            playerTwoCam.CenterOn(ConvertUnits.ToDisplayUnits(props[1].currentObject.physicsBody.Position));

            if (InputManager.IsGamePadLeftShoulderTriggered(PlayerIndex.Two)) playerTwoCam.AdjustZoom(-0.1f);
            if (InputManager.IsGamePadRightShoulderTriggered(PlayerIndex.Two)) playerTwoCam.AdjustZoom(0.1f);
        }

        public override void Draw(SpriteBatch SB)
        {
            game.GraphicsDevice.Viewport = defaultView;
            game.GraphicsDevice.Clear(new Color(48, 32, 18));

            game.GraphicsDevice.Viewport = playerOneView;
            SB.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, null, null, null, null, playerOneCam.TranslationMatrix);
            SB.Draw(testMap.texture, new Vector2(0, 0), Color.White);
            props[0].Draw(SB);
            props[1].Draw(SB);
            props[2].Draw(SB);
            testMap.Draw(SB);
            SB.End();

            SB.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, null, null, null, null, playerTwoCam.TranslationMatrix);
            game.GraphicsDevice.Viewport = playerTwoView;
            SB.Draw(testMap.texture, new Vector2(0, 0), Color.White);
            props[0].Draw(SB);
            props[1].Draw(SB);
            props[2].Draw(SB);
            testMap.Draw(SB);
            SB.End();
        }
    }
}
