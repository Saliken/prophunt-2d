﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lidgren.Network;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Utilities.ScreenSystem;
using Controller_Games.Maps;

namespace Controller_Games.Screens
{
    enum NetworkDataType
    {
        AllMapData,
        
    }

    class NetworkedHost : Screen
    {
        //Server
        NetPeerConfiguration config;
        NetServer server;
        private string ServerName;
        private string ServerPassword;

        //Game
        private Map map;

        public NetworkedHost()
        {
            config = new NetPeerConfiguration("PH2D");
            config.Port = 14242;
            config.EnableMessageType(NetIncomingMessageType.ConnectionApproval);
            server = new NetServer(config);
        }
        
        public override void Init()
        {
            server.Start();
        }

        public override void Update(GameTime gameTime)
        {
            HandleMessages(gameTime);
        }

        private void HandleMessages(GameTime gameTime)
        {
            NetIncomingMessage msg;
            while ((msg = server.ReadMessage()) != null)
            {
                switch (msg.MessageType)
                {
                    case NetIncomingMessageType.ConnectionApproval: //A client is trying to connect.
                        #region Connection Approval
                        string s = msg.ReadString();
                        if (s == ServerPassword)
                            msg.SenderConnection.Approve();
                        else
                            msg.SenderConnection.Deny();
                        #endregion
                        break;
                    case NetIncomingMessageType.DiscoveryRequest: //A client is looking for servers.
                        #region Discovery Request
                        NetOutgoingMessage response = server.CreateMessage();
                        response.Write(ServerName);
                        server.SendDiscoveryResponse(response, msg.SenderEndPoint);
                        #endregion
                        break;
                    case NetIncomingMessageType.ErrorMessage:
                        #region Error Message
                        Console.WriteLine(msg.ReadString());
                        #endregion
                        break;
                    case NetIncomingMessageType.Data: //Recieved Game Data of some kind.
                        #region Data
                        switch ((NetworkDataType)msg.ReadInt32())
                        {
                            case NetworkDataType.AllMapData: //Receive the map info, plus all objects and players currently.

                                break;
                        }
                        #endregion
                        break;
                    default:
                        Console.WriteLine("Unhandled type: " + msg.MessageType);
                        break;
                }
            }
        }

        public override void Draw(SpriteBatch SB)
        {
            throw new NotImplementedException();
        }
    }
}
