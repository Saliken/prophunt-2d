﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Newtonsoft.Json;

namespace MapEditor
{
    class AllGameObjects
    {
        public List<GameObject> allObjects;
    }

    [JsonObject(MemberSerialization.OptIn)]
    class GameObject
    {
        #region Json Properties
        [JsonProperty]
        public int id;

        [JsonProperty]
        public string name;

        [JsonProperty]
        public string textureName;

        [JsonProperty]
        public int width;

        [JsonProperty]
        public int height;

        [JsonProperty]
        public float radius;

        [JsonProperty]
        public Point Position;

        [JsonProperty]
        public float scale;

        [JsonProperty]
        public float rotation;
        #endregion

        public Texture2D Texture;
        
        

        public GameObject()
        {

        }
        
        public void CreateGameObject(Point position, ContentManager content)
        {
            Position = position;
            Texture = content.Load<Texture2D>("objects/" + textureName);
            rotation = 0f;
            scale = 1f;
        }

        public void CreateGameObject(ContentManager content)
        {
            Texture = content.Load<Texture2D>("objects/" + textureName);
        }

        public void Draw(SpriteBatch SB)
        {
            SB.Draw(Texture, new Vector2(Position.X, Position.Y), null, Color.White, rotation, new Vector2(Texture.Width / 2, Texture.Height / 2), scale, SpriteEffects.None, 1f);
        }

        public GameObject Clone()
        {
            GameObject clone = new GameObject();
            clone.id = this.id;
            clone.height = this.height;
            clone.width = this.width;
            clone.scale = this.scale;
            clone.rotation = this.rotation;
            clone.name = this.name;
            clone.Position = this.Position;
            clone.radius = this.radius;
            clone.textureName = this.textureName;
            clone.Texture = this.Texture;

            return clone;
        }

        public void Destroy()
        {
            Texture = null;
        }
    }
}
