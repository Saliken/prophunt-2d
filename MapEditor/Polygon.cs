﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using LilyPath;

namespace MapEditor
{
    class Polygon
    {
        public List<Vector2> points;

        private const float CLOSE_THRESH = 5;

        public Polygon()
        {
            points = new List<Vector2>();
        }

        public void Draw(DrawBatch DB)
        {
            if (points.Count == 1) DB.FillCircle(new SolidColorBrush(new Color(Color.Red, 0.5f)), points[0], 5f);
            else if (points.Count == 2) { DB.DrawLine(new Pen(new Color(Color.Red, 0.5f), 5f), points[0], points[1]); }
            else
            {
                for (int i = 0; i < points.Count; i++)
                {
                    DB.DrawPrimitivePath(new Pen(new Color(Color.Red, 0.5f), 5f), points);
                    DB.DrawPrimitiveLine(new Pen(new Color(Color.Red, 0.5f), 5f), points[points.Count-1], points[0]);
                }
            }
        }

        public bool IsStartPoint(Point p)
        {
            if (points.Count != 0 && Vector2.Distance(points[0], p.ToVector2()) < CLOSE_THRESH)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Used for finding the closest vector of the polygon to a given vector.
        /// </summary>
        /// <param name="vec"></param>
        /// <returns></returns>
        public int GetClosestVectorIndex(Vector2 vec)
        {
            float dist = float.MaxValue;
            int index = 0;
            for(int i = 0; i < points.Count; i++)
            {
                float vDist = Vector2.Distance(vec, points[i]);
                if(vDist < dist)
                {
                    dist = vDist;
                    index = i;
                }
            }
            return index;
        }

        /// <summary>
        /// Adds a new vector to the polygon.
        /// </summary>
        /// <param name="vec"></param>
        public void AddPointAt(Vector2 vec)
        {
            points.Insert(GetAddingIndex(vec) + 1, vec);
        }

        private int GetAddingIndex(Vector2 vector)
        {
            double dist = double.MaxValue;
            int index = 0;
            double d = 0;
            for(int i = 0; i < points.Count; i++)
            {
                if(i == points.Count - 1) d = DistanceToEdge(vector, points[i], points[0]);
                else d = DistanceToEdge(vector, points[i], points[i + 1]);

                if (d < dist)
                {
                    dist = d;
                    index = i;
                }
            }
            return index;
        }

        private double DistanceToEdge(Vector2 pos, Vector2 edge1, Vector2 edge2)
        {
            double pointX = edge2.X - edge1.X;
            double pointY = edge2.Y - edge1.Y;

            double k = pointX * pointX + pointY * pointY;
            double u = ((pos.X - edge1.X) * pointX + (pos.Y - edge1.Y) * pointY) / k;

            if (u > 1) u = 1;
            else if (u < 0) u = 0;

            double x = edge1.X + (u * pointX);
            double y = edge1.Y + (u * pointY);

            double dx = x - pos.X;
            double dy = y - pos.Y;

            return Math.Sqrt((dx * dx) + (dy * dy));
        }

        private Vector2 GetDistanceToEdge(Vector2 vector, Vector2 point1, Vector2 point2)
        {
            if (point1.X == point2.X) return new Vector2(point1.X, vector.Y);
            if (point1.Y == point2.Y) return new Vector2(vector.X, point1.Y);
            float m1 = (point2.Y - point1.Y) / (point2.X - point1.X);
            float m2 = -1 / m1;
            float x = (m1 * point1.X - m2 * vector.X + vector.Y - point1.Y) / (m1 - m2);
            float y = m2 * (x - vector.X) + vector.Y;
            return new Vector2(x, y);
        }

        public void ChangeVector(int index, Vector2 newV)
        {
            points[index] = newV;
        }

        public void AddVector(Vector2 vec)
        {
            points.Add(vec);
        }

        public void AddPoint(Point p)
        {
            points.Add(new Vector2(p.X, p.Y));
        }

        public bool IsPointWithin(Point p)
        {
            // http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html
            int i, j;
            bool c = false;
            for(i = 0, j = points.Count - 1; i < points.Count; j = i++)
            {
                if (((points[i].Y > p.Y) != (points[j].Y > p.Y)) &&
                    (p.X < (points[j].X - points[i].X) * (p.Y - points[i].Y) / (points[j].Y - points[i].Y) + points[i].X))
                    c = !c;
            }
            return c;
        }

        public bool IsVectorWithin(Vector2 p)
        {
            // http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html
            int i, j;
            bool inside = false;
            for (i = 0, j = points.Count - 1; i < points.Count; j = i++)
            {
                if (((points[i].Y > p.Y) != (points[j].Y > p.Y)) &&
                    (p.X < (points[j].X - points[i].X) * (p.Y - points[i].Y) / (points[j].Y - points[i].Y) + points[i].X))
                    inside = !inside;
            }
            return inside;
        }
    }
}
