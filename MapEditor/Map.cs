﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
using Utilities.Input;
using Newtonsoft.Json;
using System.IO;
namespace MapEditor
{
    [JsonObject(MemberSerialization.OptIn)]
    class Map
    {
        public Texture2D backgroundTex;

        [JsonProperty]
        public List<GameObject> objects;

        [JsonProperty]
        public string textureName;

        [JsonProperty]
        public List<Polygon> wallPolygons;

        [JsonProperty]
        public Vector2 propStartPosition;

        [JsonProperty]
        public Vector2 hunterStartPosition;

        private LilyPath.DrawBatch drawBatch;

        public Map()
        {
            backgroundTex = null;
            objects = new List<GameObject>();
            wallPolygons = new List<Polygon>();
        }

        public void Init(GraphicsDevice GD, ContentManager content)
        {
            drawBatch = new LilyPath.DrawBatch(GD);
            for(int i = 0; i < objects.Count; i++)
            {
                objects[i].CreateGameObject(content);
            }
        }

        public void Update(GameTime GT)
        {

        }

        public void Draw(SpriteBatch SB, Utilities.Camera2D camera)
        {
            if (backgroundTex != null) SB.Draw(backgroundTex, new Vector2(0, 0), Color.White);
            for (int i = 0; i < objects.Count; i++)
            {
                objects[i].Draw(SB);
            }

            for(int i = 0; i < wallPolygons.Count; i++)
            {
                drawBatch.Begin(LilyPath.DrawSortMode.Deferred, BlendState.AlphaBlend, null, null, null, null, camera.TranslationMatrix);
                wallPolygons[i].Draw(drawBatch);
                drawBatch.DrawPrimitiveCircle(LilyPath.Pen.Red, propStartPosition, 25f);
                drawBatch.DrawPrimitiveCircle(LilyPath.Pen.Yellow, hunterStartPosition, 25f);
                drawBatch.End();
            }
        }

        public void AddObject(GameObject obj)
        {
            objects.Add(obj);
        }
        public void RemoveObject(GameObject obj)
        {
            objects.Remove(obj);
        }
        public void AddWall(Polygon p) { wallPolygons.Add(p); }
        public void RemoveWall(Polygon p) { wallPolygons.Remove(p); }
        public Polygon GetWallAt(Point Position)
        {
            for(int i = 0; i < wallPolygons.Count; i++)
            {
                if (wallPolygons[i].IsPointWithin(Position)) return wallPolygons[i];
            }
            return null;
        }
        public void SetPropStartPosition(Vector2 Position)
        {
            propStartPosition = Position;
        }
        public void SetHunterStartPosition(Vector2 Position)
        {
            hunterStartPosition = Position;
        }
        public GameObject GetGameObjectAt(Point Position)
        {
            Rectangle bounds;
            for (int i = 0; i < objects.Count; i++)
            {
                bounds.X = objects[i].Position.X;
                bounds.Y = objects[i].Position.Y;
                bounds.Width = objects[i].Texture.Width;
                bounds.Height = objects[i].Texture.Height;
                if (bounds.Contains(Position))
                {
                    return objects[i];
                }
            }
            return null;
        }

        public void SaveMap(string name)
        {
            File.WriteAllText("Content/maps/" + name + ".json", JsonConvert.SerializeObject(this));
        }
    }
}
