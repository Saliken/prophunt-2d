﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
using Utilities.ScreenSystem;
using Utilities;
using Utilities.Input;
using Newtonsoft.Json;
using System.IO;

namespace MapEditor
{
    public enum EditorState
    {
        Idle,
        PlacingPolygonPoints,
        EditingPolygonPoints,
        PlacingObject,
        EditingObject,
        NewMap,
        LoadMap,
        SaveMap,
        PlacePropStartPoints,
        PlaceHunterStartPoints
    }


    class EditorScreen : Screen
    {
        private Random random;
        private AllGameObjects objects;
        private EditorState state;
        private Camera2D camera;
        private Map currentMap;

        //Placing Object
        private int objID;
        private GameObject objPlace;
        private string objIDString;

        //Editing Object
        private GameObject objEdit;

        //Placing Polygon
        private Polygon newPolygon;
        private LilyPath.DrawBatch drawBatch;

        //Editing Polygon
        private Polygon editPolygon;
        private int editPolygonIndex;

        //New Object
        private string newMapString;

        //Save Map
        private string saveMapString;

        //Load Map
        private string loadMapString;

        

        public override void Init()
        {
            camera = new Camera2D();
            camera.ViewportHeight = game.GraphicsDevice.Viewport.Height;
            camera.ViewportWidth = game.GraphicsDevice.Viewport.Width;

            game.Window.TextInput += TextInput;
            random = new Random();
            objects = JsonConvert.DeserializeObject<AllGameObjects>(File.ReadAllText("Content/objects/ObjectManifest.json"));
            for(int i = 0; i < objects.allObjects.Count; i++)
            {
                objects.allObjects[i].CreateGameObject(new Point(random.Next(10, 1000), random.Next(10, 1000)), Game1.content);
            }

            objID = 1;
            objIDString = "";
            objPlace = objects.allObjects[1].Clone();

            newMapString = "";
            saveMapString = "";
            currentMap = new Map();
            currentMap.Init(game.GraphicsDevice, game.Content);
            newPolygon = new Polygon();
            editPolygon = null;
            drawBatch = new LilyPath.DrawBatch(game.GraphicsDevice);
        }

        private void TextInput(object sender, TextInputEventArgs e)
        {
            //When we recieve new typing text we need to update the game based on it's state.
            switch (state)
            {
                case EditorState.Idle:
                    if (e.Character == 'p') // Place an object
                    {
                        state = EditorState.PlacingObject;
                    }
                    else if (e.Character == 'e') // Edit an object
                    {
                        objEdit = currentMap.GetGameObjectAt(camera.ScreenToWorld(InputManager.MousePositionVector()).ToPoint());
                        if (objEdit != null) state = EditorState.EditingObject;
                    }
                    else if (e.Character == 'l') // place new geometry
                    {
                        state = EditorState.PlacingPolygonPoints;
                    }
                    else if (e.Character == 'k')// Edit geometry
                    {
                        editPolygon = null;
                        editPolygonIndex = -1;
                        state = EditorState.EditingPolygonPoints;
                    }
                    else if (e.Character == 'n') // New Map CAREFUL, IT WON"T SAVE THE OLD
                    {
                        newMapString = "";
                        state = EditorState.NewMap;
                    }
                    else if (e.Character == 'b')
                    {
                        loadMapString = "";
                        state = EditorState.LoadMap;
                    }
                    else if (e.Character == 'm') //Save the map
                    {
                        saveMapString = "";
                        state = EditorState.SaveMap;
                    }
                    else if (e.Character == 'h')
                    {
                        state = EditorState.PlaceHunterStartPoints;
                    }
                    else if (e.Character == 'g')
                    {
                        state = EditorState.PlacePropStartPoints;
                    }
                    break;
                case EditorState.PlacingObject:
                    if (e.Character == '1' || e.Character == '2' || e.Character == '3' || e.Character == '4' || e.Character == '5' || e.Character == '6' || e.Character == '7' || e.Character == '8' || e.Character == '9' || e.Character == '0')
                    {
                        objIDString += e.Character;
                    }

                    if (e.Character == 'e')
                    {
                        objEdit = currentMap.GetGameObjectAt(camera.ScreenToWorld(InputManager.MousePositionVector()).ToPoint());
                        if(objEdit != null) state = EditorState.EditingObject;
                    }
                    break;
                case EditorState.EditingObject:
                    if(e.Character == 'p')
                    {
                        state = EditorState.PlacingObject;
                        objEdit = null;
                    }
                    break;
                case EditorState.NewMap:
                    newMapString += e.Character;
                    break;
                case EditorState.SaveMap:
                    saveMapString += e.Character;
                    break;
                case EditorState.LoadMap:
                    loadMapString += e.Character;
                    break;
            }

        }

        

        public override void Update(GameTime gameTime)
        {
            //Things are different depending on what state we are in
            switch (state)
            {
                case EditorState.Idle:

                    break;
                case EditorState.PlacingObject: //Place new objects
                    #region Placing Logic

                    if (InputManager.IsKeyTriggered(Keys.Space))
                    {
                        state = EditorState.Idle;
                    }

                    if(InputManager.IsKeyTriggered(Keys.Delete)) //Reset the ID
                    {
                        objIDString = "";
                        objID = 1;
                        objPlace.Destroy();
                        objPlace = objects.allObjects[objID].Clone();
                    }

                    if(objIDString != "" && objID != int.Parse(objIDString))
                    {
                        objID = int.Parse(objIDString);
                        objPlace.Destroy();
                        objPlace = objects.allObjects[objID].Clone();
                    }

                    //Update the position/orientation/scale of the current object we are placing.
                    objPlace.Position = camera.ScreenToWorld(InputManager.MousePositionVector()).ToPoint();
                    if (InputManager.IsKeyPressed(Keys.Right)) objPlace.rotation += 0.05f;
                    if (InputManager.IsKeyPressed(Keys.Left)) objPlace.rotation -= 0.05f;
                    if (InputManager.IsKeyPressed(Keys.Up)) objPlace.scale += 0.005f;
                    if (InputManager.IsKeyPressed(Keys.Down)) objPlace.scale -= 0.005f;

                    if(InputManager.IsMouseButtonTriggered(MouseButtons.Left)) //Place the object
                    {
                        currentMap.AddObject(objPlace);
                        objPlace = objPlace.Clone(); //Clone the object.
                    }
                    #endregion 
                    break;
                case EditorState.EditingObject: //Edit existing objects
                    #region Editing Logic
                    if (InputManager.IsMouseButtonPressed(MouseButtons.Left))
                    {
                        objEdit.Position = camera.ScreenToWorld(InputManager.MousePositionVector()).ToPoint();
                    }
                    if (InputManager.IsKeyPressed(Microsoft.Xna.Framework.Input.Keys.Right)) objEdit.rotation += 0.05f;
                    if (InputManager.IsKeyPressed(Microsoft.Xna.Framework.Input.Keys.Left)) objEdit.rotation -= 0.05f;
                    if (InputManager.IsKeyPressed(Microsoft.Xna.Framework.Input.Keys.Up)) objEdit.scale += 0.005f;
                    if (InputManager.IsKeyPressed(Microsoft.Xna.Framework.Input.Keys.Down)) objEdit.scale -= 0.005f;
                    
                    if (InputManager.IsKeyTriggered(Microsoft.Xna.Framework.Input.Keys.Delete)) //Remove the object
                    {
                        objEdit.Destroy();
                        currentMap.RemoveObject(objEdit);
                        objEdit = null;
                        state = EditorState.Idle;
                    }
                    #endregion
                    break;
                case EditorState.NewMap:
                    #region New Map Logic
                    if (InputManager.IsKeyTriggered(Keys.Enter))
                    {
                        //Try and load the mapImage. If it fails then bail out.
                        //If it succeeds then we need to create a new map:

                        try
                        {
                            string st = "Content/maps/" + newMapString.TrimEnd('\r', '\n');
                            Texture2D mapTex = Texture2D.FromStream(game.GraphicsDevice, File.OpenRead(st));
                            currentMap.backgroundTex = mapTex;
                            currentMap.textureName = newMapString.TrimEnd('\r', '\n');
                        }
                        catch
                        {
                            Console.WriteLine("Invalid image name/location!");
                        }
                        //.. Create a boundary based on the images bounds
                        //.. New empty list of objects
                        //.. New empty list of polygons

                        state = EditorState.Idle;
                        
                    }
                    #endregion
                    break;
                case EditorState.SaveMap:
                    #region Save Map Logic
                    if(InputManager.IsKeyTriggered(Keys.Enter))
                    {
                        try
                        {
                            string saveName = saveMapString.TrimEnd('\r', '\n');
                            currentMap.SaveMap(saveName);
                        }
                        catch (Exception e)
                        {

                        }
                        state = EditorState.Idle;
                    }
                    #endregion
                    break;
                case EditorState.PlacingPolygonPoints:
                    #region Placing Polygon Logic

                    if(InputManager.IsKeyPressed(Keys.Tab))
                    {
                        newPolygon = new Polygon();
                        state = EditorState.Idle;
                    }
                    else if (InputManager.IsKeyTriggered(Keys.Space)) //End the polygon
                    {
                        currentMap.AddWall(newPolygon);
                        newPolygon = new Polygon();
                    }
                    else if (InputManager.IsMouseButtonTriggered(MouseButtons.Left)) //Place a new point
                    {
                        if(newPolygon.IsStartPoint(camera.ScreenToWorld(InputManager.MousePositionVector()).ToPoint())) //End the polygon
                        {
                            currentMap.AddWall(newPolygon);
                            newPolygon = new Polygon();
                        }
                        else newPolygon.AddVector(camera.ScreenToWorld(InputManager.MousePositionVector()));
                    }
                    #endregion
                    break;
                case EditorState.EditingPolygonPoints:
                    #region Editing Polygon Logic
                    //We have the polygon clicked on. Now we check for the closest point and move it around
                    if (InputManager.IsMouseButtonTriggered(MouseButtons.Right)) { editPolygon = null; editPolygonIndex = 0; }
                    if(InputManager.IsKeyTriggered(Keys.Tab))
                    {
                        state = EditorState.Idle;
                        editPolygon = null;
                        editPolygonIndex = 0;
                    }

                    if (editPolygon == null)
                    {
                        if (InputManager.IsMouseButtonTriggered(MouseButtons.Left))
                        {
                            editPolygon = currentMap.GetWallAt(camera.ScreenToWorld(InputManager.MousePositionVector()).ToPoint());
                        }
                    }
                    else
                    {
                        if (InputManager.IsMouseButtonTriggered(MouseButtons.Left)) //Look for a new point
                        {
                            editPolygonIndex = editPolygon.GetClosestVectorIndex(camera.ScreenToWorld(InputManager.MousePositionVector()));
                        }

                        if (editPolygonIndex != -1 && InputManager.IsMouseButtonPressed(MouseButtons.Left)) //Move the current point
                        {
                            editPolygon.ChangeVector(editPolygonIndex, camera.ScreenToWorld(InputManager.MousePositionVector()));
                        }
                        else if (InputManager.IsKeyTriggered(Keys.N))
                        {
                            editPolygon.AddPointAt(camera.ScreenToWorld(InputManager.MousePositionVector()));
                        }
                    }
                    #endregion 
                    break;
                case EditorState.LoadMap:
                    #region Load Map Logic
                    if(InputManager.IsKeyTriggered(Keys.Enter)) //Load the map
                    {
                        try
                        {
                            string name = "Content/maps/" + loadMapString.TrimEnd('\r', '\n');
                            currentMap = JsonConvert.DeserializeObject<Map>(File.ReadAllText(name));
                            Texture2D mapTex = Texture2D.FromStream(game.GraphicsDevice, File.OpenRead("Content/maps/" + currentMap.textureName));
                            currentMap.backgroundTex = mapTex;
                            currentMap.Init(game.GraphicsDevice, game.Content);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("Error loading map:" + e.ToString());
                        }
                        state = EditorState.Idle;
                    }
                    #endregion
                    break;
                case EditorState.PlaceHunterStartPoints:
                    #region Place Hunter Start Point Logic
                    if(InputManager.IsMouseButtonPressed(MouseButtons.Left))
                    {
                        currentMap.SetHunterStartPosition(camera.ScreenToWorld(InputManager.MousePositionVector()));
                    }

                    if (InputManager.IsKeyTriggered(Keys.Tab))
                    {
                        state = EditorState.Idle;
                    }
                    #endregion
                    break;
                case EditorState.PlacePropStartPoints:
                    #region Place Prop Start Point Logic
                    if (InputManager.IsMouseButtonPressed(MouseButtons.Left))
                    {
                        currentMap.SetPropStartPosition(camera.ScreenToWorld(InputManager.MousePositionVector()));
                    }

                    if (InputManager.IsKeyTriggered(Keys.Tab))
                    {
                        state = EditorState.Idle;
                    }
                    #endregion
                    break;
                default:
                    break;
            }

            #region Camera Transformations
            Vector2 movement = Vector2.Zero;

            if (InputManager.IsKeyPressed(Keys.W))
            {
                movement.Y = -1;
            }
            if (InputManager.IsKeyPressed(Keys.A))
            {
                movement.X = -1;
            }
            if (InputManager.IsKeyPressed(Keys.S))
            {
                movement.Y = 1;
            }
            if (InputManager.IsKeyPressed(Keys.D))
            {
                movement.X = 1;
            }
            if (InputManager.IsKeyPressed(Keys.OemPlus)) camera.AdjustZoom(0.07f);
            if (InputManager.IsKeyPressed(Keys.OemMinus)) camera.AdjustZoom(-0.07f);

            camera.MoveCamera(movement * 5);
            #endregion
        }

        public override void Draw(SpriteBatch SB)
        {
            SB.Begin(SpriteSortMode.Immediate, null, null, null, null, null, camera.TranslationMatrix);

            //Draw the map and map objects
            currentMap.Draw(SB, camera);
            //Draw the brush
            if(state == EditorState.PlacingObject) objPlace.Draw(SB);
            SB.End();

            drawBatch.Begin(LilyPath.DrawSortMode.Deferred, BlendState.AlphaBlend, null, null, null, null, camera.TranslationMatrix);
            if (state == EditorState.PlacingPolygonPoints) newPolygon.Draw(drawBatch);
            drawBatch.End();
        }
    }
}
